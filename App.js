import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  FlatList,
  AsyncStorage,
  ActivityIndicator,
  Platform
} from "react-native";
import {
  createStackNavigator,
  createDrawerNavigator,
  createAppContainer,
  NavigationActions,
  NavigationAction
} from "react-navigation";

import SplashScreen from "./src/app/screens/SplashScreen";
import Login from "./src/app/screens/Login";
import Account from "./src/app/screens/Akun/Account";
import SettingAccount from "./src/app/screens/Akun/SettingAccount";
import HomeMenu from "./src/app/screens/HomeMenu";
import ListClient from "./src/app/screens/Client/ListClient";
import DetailClient from "./src/app/screens/Client/DetailClient";
import ListHistory from "./src/app/screens/Client/ListHistory";
import DetailHistory from "./src/app/screens/Client/DetailHistory";


const NavStack = createStackNavigator({
  SplashScreen: {
    screen: SplashScreen
  },
  Login: {
    screen: Login
  },
  Account: {
    screen: Account
  },
  SettingAccount: {
    screen: SettingAccount
  },
  HomeMenu: {
    screen: HomeMenu
  },
  ListClient: {
    screen: ListClient
  },
  DetailClient: {
    screen: DetailClient
  },
  ListHistory: {
    screen: ListHistory
  },
  DetailHistory: {
    screen: DetailHistory
  }

});


const App = createAppContainer(NavStack);

export default App;
