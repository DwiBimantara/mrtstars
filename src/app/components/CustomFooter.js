import React, { Component } from "react";
import { Platform,Image,AsyncStorage } from "react-native";
import { Footer, FooterTab, Text, Button, Icon, View, Badge } from "native-base";

import styles from "../screens/styles/CustomFooter";
import colors from "../../styles/colors";

class CustomFooter extends Component {

  componentDidMount() {

  }

  render() {
    return (
      <View>
      {(<Footer>
        <FooterTab style={{ backgroundColor: "#ebebeb"}}>
        {this.props.menu == "Home" ? (
          <Button
            vertical
            onPress={() => this.props.navigation.navigate("HomeMenu")}
          >
              <Image
                source={require("../../assets/images/Home-Color.png")}
                style={{ width: 25, height: 25, resizeMode: "contain" }}
              />
              <Text style={{ color: colors.secondary, fontSize:10}}>Home</Text>
          </Button>
        ):(
          <Button
            vertical
            onPress={() => this.props.navigation.navigate("HomeMenu")}
          >
              <Image
                source={require("../../assets/images/Home.png")}
                style={{ width: 25, height: 25, resizeMode: "contain" }}
              />
              <Text style={{ color: "#808080", fontSize:10 }}>Home</Text>
          </Button>
        )}
          {this.props.menu == "Account" ? (
            <Button
              vertical
              // active
              onPress={() => this.props.navigation.navigate("Account")}
              // style={{color:'#808080', fontSize:10, backgroundColor: '#27ae60'}}
            >
              <Image
                source={require("../../assets/images/Profil-Color.png")}
                style={{ width: 25, height: 25, resizeMode: "contain" }}
              />
              <Text style={{  color: colors.secondary, fontSize:10 }}>Account</Text>
            </Button>
          ) : (
            <Button
              vertical
              onPress={() => this.props.navigation.navigate("Account")}
            >
              <Image
                source={require("../../assets/images/Profil.png")}
                style={{ width: 25, height: 25, resizeMode: "contain" }}
              />
              <Text style={{ color: "#808080", fontSize:10 }}>Account</Text>
            </Button>
          )}
        </FooterTab>
      </Footer>)}
      </View>
    );
  }
}

export default CustomFooter;