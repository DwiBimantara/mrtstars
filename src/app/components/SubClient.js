import React, { Component } from "react";
import { View, Text, Image, TouchableNativeFeedback } from "react-native";
import { Icon } from "native-base";
import colors from "../../styles/colors";
import PropTypes from "prop-types";
 
let styles = {
  container: {
    flex: 1,
    height: 180,
    width: 300,
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 0,
    borderRadius:10,
    backgroundColor:colors.white,
  },
  b: {
    backgroundColor: colors.primary
  }
};
 
export default class SubClient extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
 
  render() {
    const { loading, disabled, handleOnPress } = this.props;
    return (
      <View style={[styles.container]}>
        <View style={{width:290, height:170}}>
          <Image
            source={require("../../assets/images/Profil.png")}/>
            style={{ width: 290, height:'100%', marginTop: 0, marginBottom: 2, borderRadius:10 }}
          />
        </View>
        <View style={{ flex: 1, width:290, paddingLeft:10, paddingRight:5, borderBottomRightRadius:10, borderBottomLeftRadius:10, marginTop:-45}}>
            <Text style={{fontSize:13, fontWeight: "bold", color:colors.white}}>{this.props.nama}</Text>
            <Text style={{fontSize:11, color:colors.white}}>{this.props.pekerjaan}</Text>
        </View>
      </View>
    );
  }
}
 
 
SubClient.propTypes = {
  handleOnPress: PropTypes.func,
  disabled: PropTypes.bool
};