import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  BackHandler,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon
} from "native-base";
import CustomFooter from "../../components/CustomFooter";
import styles from "../styles/Account";
import colors from "../../../styles/colors";
import Moment from "moment";

export default class Account extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      dataUser: [],
      email: "",
      id: "",
      nama: "",
      tgl_lahir: "",
      code_agent: ""
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    //BackHandler.addEventListener("hardwareBackPress", this.backPressed);
    AsyncStorage.getItem("profil").then(dataUser => {
      //alert(JSON.parse(dataUser).nama)
      this.setState({
        id: JSON.parse(dataUser).id_client,
        nama: JSON.parse(dataUser).nama,
        tgl_lahir: JSON.parse(dataUser).tgl_lahir,
        email: JSON.parse(dataUser).email,
        code_agent: JSON.parse(dataUser).code_agent
      });
    });
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.backPressed);
  }

  backPressed = () => {
    this.props.navigation.goBack(null);
    return true;
  };

  // loadData(id){
  //   var url = GlobalConfig.SERVERHOST + 'getUserId/'+id;

  //   fetch(url, {
  //     headers: {
  //       'Content-Type': 'multipart/form-data'
  //     },
  //     method: 'GET',
  //   }).then((response) => response.json())
  //     .then((response) => {
  //         if(response.code == 302) {
  //           this.setState({
  //             nama: response.data.nama,
  //             pekerjaan: response.data.pekerjaan,
  //             email: response.data.email,
  //             tgl_lahir: response.data.tgl_lahir,
  //             telp: response.data.telp,
  //           });
  //         }
  //     })
  // }

  exitApp() {
    Alert.alert(
      "Confirmation",
      "Logout Financial Checkup Mobile?",
      [
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "Yes", onPress: () => this.exitApplication() }
      ],
      { cancelable: false }
    );
  }
  exitApplication() {
    this.props.navigation.navigate("Login");
  }

  render() {
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
        <ScrollView>
          <Content style={{ marginTop: "20%" }}>
            <View
              style={{
                width: "100%",
                paddingLeft: 10,
                paddingRight: 10,
                marginTop: "10%"
              }}
            >
              <View
                style={{
                  borderRadius: 10,
                  borderWidth: 1,
                  borderColor: colors.graydark,
                  flex: 1,
                  flexDirection: "row"
                }}
              >
                <View
                  style={{
                    width: "65%",
                    flex: 1,
                    flexDirection: "row",
                    paddingTop: 5,
                    paddingLeft: 5,
                    paddingBottom: 5
                  }}
                >
                  <View style={{ width: "30%" }}>
                    <Image
                      style={{
                        width: 90,
                        height: 90,
                        borderRadius: 20
                      }}
                      source={require("../../../assets/images/Profil.png")}
                    ></Image>
                  </View>
                  <View style={{ width: "70%" }}>
                    {/* <Text style={{fontSize:12, paddingTop:0, fontWeight:'bold', color:colors.black}}>{this.state.nama}</Text> */}
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      {/* <Icon
                        name='ios-pin'
                        style={{fontSize:15, color:colors.black, paddingRight:5}}
                    /> */}
                      <Text style={{ fontSize: 12, paddingTop: 0 }}>
                        Nama{"\t\t\t\t"}: {this.state.nama}
                      </Text>
                    </View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      {/* <Icon
                          name='mail-open'
                          style={{fontSize:15, color:colors.black, paddingRight:5}}
                      /> */}
                      <Text style={{ fontSize: 12, paddingTop: 0 }}>
                        Tanggal Lahir{"\t"}:{" "}
                        {Moment(this.state.tgl_lahir).format("DD MMMM YYYY")}
                      </Text>
                    </View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      {/* <Icon
                          name='mail-open'
                          style={{fontSize:15, color:colors.black, paddingRight:5}}
                      /> */}
                      <Text style={{ fontSize: 12, paddingTop: 0 }}>
                        Code Agent{"\t\t"}: {this.state.code_agent}
                      </Text>
                    </View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      {/* <Icon
                          name='mail-open'
                          style={{fontSize:15, color:colors.black, paddingRight:5}}
                      /> */}
                      <Text style={{ fontSize: 12, paddingTop: 0 }}>
                        Email{"\t\t\t\t"}: {this.state.email}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
            {/* <TouchableOpacity
            transparent
            onPress={() =>
              this.props.navigation.navigate("SettingAccount")
              }
          >
          <View style={{width:'100%', paddingLeft:10, paddingRight:10, marginTop:"10%"}}>
            <View style={{ flex:1, flexDirection:'row', paddingTop:5, paddingBottom:5}}>
              <View style={{width:'10%', flex:1, flexDirection:'row', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center", alignItems: "center"}}>

                <Icon
                    name='ios-contact'
                    style={{fontSize:25, color:'#000000'}}
                />

              </View>
              <View style={{width:'80%', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center"}}>
                <Text style={{fontSize:12}}>Account Setting</Text>
              </View>
              <View style={{width:'10%', paddingTop:5, paddingLeft:5, paddingBottom:5, justifyContent: "center", alignItems: "center"}}>
                <Icon
                    name='ios-play'
                    style={{fontSize:20, color:colors.graydark}}
                />
              </View>
            </View>
          </View>
          </TouchableOpacity> */}
            <TouchableOpacity transparent onPress={() => this.exitApp()}>
              <View
                style={{
                  width: "100%",
                  paddingLeft: 10,
                  paddingRight: 10,
                  marginTop: "5%"
                }}
              >
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    paddingTop: 5,
                    paddingBottom: 5
                  }}
                >
                  <View
                    style={{
                      width: "10%",
                      flex: 1,
                      flexDirection: "row",
                      paddingTop: 5,
                      paddingLeft: 5,
                      paddingBottom: 5,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Icon
                      name="log-out"
                      style={{ fontSize: 25, color: "#319F4C" }}
                    />
                  </View>
                  <View
                    style={{
                      width: "80%",
                      paddingTop: 5,
                      paddingLeft: 5,
                      paddingBottom: 5,
                      justifyContent: "center"
                    }}
                  >
                    <Text style={{ fontSize: 12 }}>Logout</Text>
                  </View>
                  <View
                    style={{
                      width: "10%",
                      paddingTop: 5,
                      paddingLeft: 5,
                      paddingBottom: 5,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Icon
                      name="ios-play"
                      style={{ fontSize: 20, color: colors.graydark }}
                    />
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          </Content>
        </ScrollView>
        <CustomFooter navigation={this.props.navigation} menu="Account" />
      </Container>
    );
  }
}
