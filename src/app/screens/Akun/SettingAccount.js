import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Card,
  CardItem,
  Form,
  Item,
  Label,
  Input,
} from "native-base";

import styles from "./../styles/Login";
import colors from "../../../styles/colors";
import GlobalConfig from "../../components/GlobalConfig";

export default class SettingAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id:'',
      nama:'',
      email:'',
      pekerjaan:'',
      tgl_lahir:'',
      telp:'',
      password:'',
      dataUser:'',
      uri: '',
    };
  }

  static navigationOptions = {
    header: null
  };


  componentDidMount() {
      AsyncStorage.getItem('profil').then((dataUser) => {
        this.loadData(JSON.parse(dataUser).id);
      })
  }


  loadData(id){
    var url = GlobalConfig.SERVERHOST + 'getUserId/'+id;
    fetch(url, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      method: 'GET',
    }).then((response) => response.json())
      .then((response) => {
          if(response.code == 302) {
            this.setState({
              id:response.data.id,
              nama: response.data.nama,
              pekerjaan: response.data.pekerjaan,
              email: response.data.email,
              tgl_lahir: response.data.tgl_lahir,
              telp: response.data.telp,
            });
          }
      })
  }



  konfirmasiUpdate(){
    if(this.state.nama==""){
      Alert.alert(
        'Information',
        'Input Full Name',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    }
    else if(this.state.pekerjaan==""){
      Alert.alert(
        'Information',
        'Input Pekerjaan',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    }
    else if(this.state.email==""){
      Alert.alert(
        'Information',
        'Input Email',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
    }
    else if(this.state.tgl_lahir==""){
        Alert.alert(
          'Information',
          'Input Tanggal Lahir',
          [
            { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
          ],
          { cancelable: false }
        );
    }
    else if(this.state.telp==""){
        Alert.alert(
          'Information',
          'Input Telp',
          [
            { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
          ],
          { cancelable: false }
        );
    } else {

          var url = GlobalConfig.SERVERHOST + 'update-profile/'+this.state.id;
          var formData = new FormData();
          formData.append("nama", this.state.nama)
          formData.append("pekerjaan", this.state.pekerjaan)
          formData.append("tgl_lahir", this.state.tgl_lahir)
          formData.append("telp", this.state.telp)
          formData.append("email", this.state.email)
          formData.append("password", this.state.password)
          fetch(url, {
            headers: {
              'Content-Type': 'multipart/form-data'
            },
            method: 'POST',
            body: formData
          }).then((response) => response.json())
            .then((response) => {
              if(response.code == 302) {
                Alert.alert('Success', 'Update Profile Success', [{
                  text: 'OK'
                }]);
                this.props.navigation.navigate("HomeMenu");
              } else{
                Alert.alert('Error', 'Update Profile Failed', [{
                  text: 'OK'
                }]);
              }
          });
      }
  }

  render() {
    return this.state.isloading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <View style={styles.homeWrapper}>
        <View
          style={{ flex: 1, flexDirection: "column", backgroundColor: "#fff", width:'100%' }}
        >
          <View>
            <ScrollView>
              <Card style={{ marginLeft: 20, marginRight: 20, borderRadius: 20, marginTop:"20%", paddingBottom:40 }}>
              <View>
                <Text style={styles.signIn}>UPDATE PROFILE</Text>
              </View>
              <Form style={{ marginLeft: 10, marginRight:20, marginTop:5}}>
                <Item stackedLabel>
                  <Label style={styles.weatherText}>Full Name</Label>
                  <View style={{flex:1, flexDirection:'row'}}>
                      <Icon
                        name='contact'
                        style={{color:colors.black, fontSize:25, marginTop:12}}
                      />
                      <Input value={this.state.nama} onChangeText={(text) => this.setState({ nama: text })} />
                  </View>
                </Item>
                <Item stackedLabel>
                  <Label style={styles.weatherText}>Pekerjaan</Label>
                  <View style={{flex:1, flexDirection:'row'}}>
                      <Icon
                        name='mail-open'
                        size={10}
                        style={{color:colors.black, fontSize:25, marginTop:12}}
                      />
                      <Input value={this.state.pekerjaan} onChangeText={(text) => this.setState({ pekerjaan: text })} />
                  </View>
                </Item>
                <Item stackedLabel>
                  <Label style={styles.weatherText}>Tanggal Lahir</Label>
                  <View style={{flex:1, flexDirection:'row'}}>
                      <Icon
                        name='mail-open'
                        size={10}
                        style={{color:colors.black, fontSize:25, marginTop:12}}
                      />
                      <Input value={this.state.tgl_lahir} onChangeText={(text) => this.setState({ tgl_lahir: text })} />
                  </View>
                </Item>
                <Item stackedLabel>
                  <Label style={styles.weatherText}>Telepon</Label>
                  <View style={{flex:1, flexDirection:'row'}}>
                      <Icon
                        name='mail-open'
                        size={10}
                        style={{color:colors.black, fontSize:25, marginTop:12}}
                      />
                      <Input value={this.state.telp} onChangeText={(text) => this.setState({ telp: text })} />
                  </View>
                </Item>
                <Item stackedLabel>
                  <Label style={styles.weatherText}>Email</Label>
                  <View style={{flex:1, flexDirection:'row'}}>
                      <Icon
                        name='mail-open'
                        size={10}
                        style={{color:colors.black, fontSize:25, marginTop:12}}
                      />
                      <Input value={this.state.email} onChangeText={(text) => this.setState({ email: text })} />
                  </View>
                </Item>
                <Item stackedLabel>
                  <Label style={styles.weatherText}>Password</Label>
                  <View style={{flex:1, flexDirection:'row'}}>
                      <Icon
                        name='mail-open'
                        size={10}
                        style={{color:colors.black, fontSize:25, marginTop:12}}
                      />
                      <Input secureTextEntry={true} onChangeText={(text) => this.setState({ password: text })} />
                  </View>
                </Item>
              </Form>
              </Card>
              <CardItem style={{ borderRadius: 0, marginTop:10}}>
                <View style={{ flex: 1, flexDirection:'column'}}>
                <View style={styles.Contentsave}>
                  <Button
                    block
                    style={{
                      width:'100%',
                      height: 45,
                      marginBottom: 10,
                      borderWidth: 0,
                      backgroundColor: colors.primarydark,
                      borderRadius: 15
                    }}
                    onPress={() => this.konfirmasiUpdate()}
                  >
                    <Text style={{color:colors.white}}>Update Profile</Text>
                  </Button>
                </View>
                </View>
              </CardItem>
            </ScrollView>
          </View>
        </View>
        </View>
        </Container>
      );
  }
}