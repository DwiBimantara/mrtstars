import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Card
} from "native-base";
import colors from "../../../styles/colors";
import styles from "../styles/AllClient";
import Moment from "moment";

export default class DetailClient extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      isLoading: false,
      listClient: [],
      id_client: "",
      nama: "",
      pekerjaan: "",
      tgl_lahir: "",
      email: "",
      telp: "",
      id: ""
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    AsyncStorage.getItem("listClient").then(listClient => {
      //alert(JSON.parse(listClient).id_client);
      this.setState({
        listClient: listClient,
        id: JSON.parse(listClient).id_client,
        nama: JSON.parse(listClient).nama,
        pekerjaan: JSON.parse(listClient).pekerjaan,
        alamat: JSON.parse(listClient).alamat,
        tgl_lahir: JSON.parse(listClient).tgl_lahir,
        email: JSON.parse(listClient).email,
        telp: JSON.parse(listClient).telp
      });
    });
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {}
    );
  }

  navigateToScreen(route, idClient) {
    AsyncStorage.setItem("idClient", JSON.stringify(idClient)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  render() {
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <TouchableOpacity
              transparent
              onPress={() => this.props.navigation.goBack()}
            >
              <Icon
                name="arrow-back"
                size={35}
                style={{ color: colors.black }}
              />
            </TouchableOpacity>
          </Left>
          <Right style={{ flex: 1 }} />
        </Header>
        <ScrollView>
          <Content style={{ marginTop: 0 }}>
            <View style={{ width: "100%", height: 280 }}>
              <Image
                style={{
                  marginTop: "10%",
                  alignSelf: "center",
                  width: "40%",
                  height: "40%",
                  borderBottomRightRadius: 50
                }}
                source={require("../../../assets/images/Profil.png")}
              />
            </View>
            <Card
              style={{
                marginLeft: 20,
                marginRight: 20,
                borderRadius: 20,
                marginTop: "-20%",
                paddingBottom: 20
              }}
            >
              <View
                style={{
                  width: "100%",
                  paddingLeft: 10,
                  paddingRight: 10,
                  marginTop: 10
                }}
              >
                <Text
                  style={{
                    fontSize: 15,
                    color: colors.black,
                    fontWeight: "bold"
                  }}
                >
                  Nama {"\t\t\t\t"}: {"\t" + this.state.nama}
                </Text>
                <Text style={{ fontSize: 17, paddingTop: 0 }}>
                  Pekerjaan{"\t\t"}: {"\t" + this.state.pekerjaan}
                </Text>
                <Text style={{ fontSize: 15, paddingTop: 0 }}>
                  Alamat{"\t\t\t\t"}: {"\t" + this.state.alamat}
                </Text>
                <Text style={{ fontSize: 15, paddingTop: 0 }}>
                  Tanggal Lahir{"\t"}:{"\t "}
                  {Moment(this.state.tgl_lahir).format("DD MMMM YYYY")}
                </Text>
                <Text style={{ fontSize: 15, paddingTop: 0 }}>
                  Email{"\t\t\t\t\t"}: {"\t" + this.state.email}
                </Text>
                <Text style={{ fontSize: 15, paddingTop: 0 }}>
                  Telp{"\t\t\t\t\t"}: {"\t" + this.state.telp}
                </Text>
              </View>
            </Card>
            <View
              style={{
                width: "100%",
                justifyContent: "center",
                alignItems: "center",
                marginTop: 40
              }}
            >
              <View
                style={{
                  width: 80,
                  height: 80,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <TouchableOpacity
                  transparent
                  onPress={() =>
                    this.navigateToScreen("ListHistory", this.state.id)
                  }
                >
                  <Image
                    source={require("../../../assets/images/history.png")}
                    style={{ width: 60, height: 60, resizeMode: "contain" }}
                  />
                  <Text style={{ fontSize: 15 }}>History</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Content>
        </ScrollView>
      </Container>
    );
  }
}
