import React, { Component } from "react";
import {
  Collapse,
  CollapseHeader,
  CollapseBody
} from "accordion-collapse-react-native";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Thumbnail,
  List,
  ListItem,
  Separator,
  CardItem
} from "native-base";
import styles from "../styles/AllClient";
import colors from "../../../styles/colors";
import GlobalConfig from "../../components/GlobalConfig";
import ListHistory from "./ListHistory";
import Moment from "moment";
import ListClient from "./ListClient";

var that;
class ListItem2 extends React.PureComponent {
  render() {
    return (
      <View>
        <Collapse>
          {this.props.data.ratio ==
          "Perbandingan Nilai Bersih Aset Investasi terhadap Nilai Bersih Kekayaan" ? (
            <CollapseHeader style={{ height: 55 }}>
              <Separator bordered>
                <View>
                  <Text
                    style={{
                      fontWeight: "bold",
                      marginBottom: -5,
                      paddingRight: 40
                    }}
                  >
                    {this.props.data.ratio}
                    {"\t"}
                    <Icon
                      type="AntDesign"
                      name="caretdown"
                      style={{
                        color: colors.black,
                        fontSize: 10,
                        marginTop: -10
                      }}
                    />
                  </Text>
                </View>
              </Separator>
            </CollapseHeader>
          ) : (
            <CollapseHeader style={{ height: 40 }}>
              <Separator bordered>
                <View>
                  <Text style={{ fontWeight: "bold" }}>
                    {this.props.data.ratio}
                    {"\t"}

                    <Icon
                      type="AntDesign"
                      name="caretdown"
                      style={{
                        color: colors.black,
                        fontSize: 10,
                        marginTop: -10
                      }}
                    />
                  </Text>
                </View>
              </Separator>
            </CollapseHeader>
          )}
          <CollapseBody>
            {this.props.data.ratio == "Likuiditas" ? (
              <Text style={{ marginLeft: 10, marginTop: 10, fontSize: 20 }}>
                Nilai : {this.props.data.value} Bulan
              </Text>
            ) : (
              <Text style={{ marginLeft: 10, marginTop: 10, fontSize: 20 }}>
                Nilai : {this.props.data.value} %
              </Text>
            )}

            <Text style={{ marginLeft: 10, fontWeight: "bold", fontSize: 17 }}>
              {"\n"}
              <Icon
                type="AntDesign"
                name="filetext1"
                style={{ color: "#5fa81e", fontSize: 20, marginTop: -10 }}
              />{" "}
              Statement :
            </Text>
            <Text style={{ marginLeft: 10, fontSize: 17 }}>
              {this.props.data.rule_statement}
            </Text>

            <Text style={{ marginLeft: 10, fontWeight: "bold", fontSize: 17 }}>
              {"\n"}
              <Icon
                type="Foundation"
                name="lightbulb"
                style={{ color: "#e0cd34", fontSize: 30, marginTop: -10 }}
              />{" "}
              Saran :
            </Text>
            <Text style={{ marginLeft: 10, fontSize: 17 }}>
              {this.props.data.rule_saran}
              {"\n"}
            </Text>
          </CollapseBody>
        </Collapse>
      </View>
    );
  }
}

export default class DetailHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      active: "true",
      isLoading: false,
      idClient: "",
      id: "",
      date: "",
      valueHistory: []
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    AsyncStorage.getItem("listClient").then(listClient => {
      //alert(JSON.parse(listClient).id_client)
      AsyncStorage.getItem("date").then(date => {
        this.setState({
          id: JSON.parse(listClient).id_client,
          date: JSON.parse(date)
        });
        this.detail();
      });
    });
  }

  detail() {
    var url =
      GlobalConfig.SERVERHOST + "detail-history-client/" + this.state.id;
    var formData = new FormData();
    //alert(JSON.stringify(this.state.listHistory.day))
    formData.append("date", this.state.date);

    fetch(url, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      method: "POST",
      body: formData
    })
      .then(responseJson => responseJson.json())
      .then(responseJson => {
        //alert(JSON.stringify(responseJson))
        if (responseJson.code == 302) {
          this.setState({
            valueHistory: responseJson.dataRule,
            isLoading: false
          });
        } else {
          Alert.alert("Error", "Create Product Failed", [
            {
              text: "OK"
            }
          ]);
        }
      });
  }

  _renderItem = ({ item }) => <ListItem2 data={item} />;

  render() {
    that = this;
    return (
      <Container style={styles.wrapper}>
        <StatusBar backgroundColor="#000000" barStyle="light-content" />
        {/* <View>
    <StatusBar hidden={route.statusBarHidden} />
    ...
  </View> */}
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon
                name="arrow-back"
                style={{ color: colors.black, fontSize: 30 }}
              />
            </Button>
          </Left>
          <Body style={{ flex: 3, alignItems: "center" }}>
            <Title style={{ fontSize: 15, color: colors.black }}>
              Detail History
            </Title>
          </Body>
          <Right style={{ flex: 1 }}></Right>
        </Header>
        <CardItem>
          <View style={{ alignItems: "center" }}>
            <Text>
              Tanggal : {Moment(this.state.date).format("DD MMMM YYYY")}
            </Text>
          </View>
        </CardItem>

        <ScrollView>
          <Content style={{ marginTop: 0 }}>
            <View style={{ width: "100%", paddingLeft: 0, paddingRight: 0 }}>
              <FlatList
                data={this.state.valueHistory}
                renderItem={this._renderItem}
                numColumns={1}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
          </Content>
        </ScrollView>
      </Container>
    );
  }
}
