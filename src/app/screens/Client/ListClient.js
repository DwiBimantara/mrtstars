import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput
} from "react-native";
import {
  Container,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Header
} from "native-base";
import styles from "../styles/AllClient";
import colors from "../../../styles/colors";
import GlobalConfig from "../../components/GlobalConfig";

var that;
class ListItem extends React.PureComponent {
  navigateToScreen(route, listClient) {
    AsyncStorage.setItem("listClient", JSON.stringify(listClient)).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View style={[styles.container]}>
        <TouchableOpacity
          transparent
          onPress={() => this.navigateToScreen("DetailClient", this.props.data)}
        >
          <View style={{ flexDirection: "row", marginLeft: 5 }}>
            <Icon
              type="FontAwesome5"
              name="user-circle"
              style={{ color: "#212e59", fontSize: 30, marginTop: 5 }}
            />
            <View
              style={{
                width: "100%",
                paddingTop: 3,
                paddingLeft: 10,
                paddingRight: 10,
                marginBottom: 5
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  color: colors.black,
                  fontWeight: "bold"
                }}
              >
                Nama{"\t\t\t"}: {this.props.data.nama}
              </Text>
              <Text
                style={{
                  fontSize: 12,
                  paddingTop: 5,
                  fontWeight: "bold",
                  color: colors.secondary
                }}
              >
                Pekerjaan{"\t"}: {this.props.data.pekerjaan}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

export default class ListClient extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      active: "true",
      isLoading: false,
      search: "Search",
      listClient: [],
      filter: "",
      idClient: ""
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount(text) {
    AsyncStorage.getItem("idClient").then(idClient => {
      if (text == undefined || text == null || text == "") {
        var url = GlobalConfig.SERVERHOST + "list-client/" + idClient;
      } else {
        var url =
          GlobalConfig.SERVERHOST +
          "list-client-search/" +
          idClient +
          "?query=" +
          text;
      }

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "GET"
      })
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.code == 302) {
            this.setState({
              listClient: responseJson.data,
              isLoading: false
            });
          }
        });
    });

    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {}
    );
  }

  _renderItem = ({ item }) => <ListItem data={item} />;

  render() {
    that = this;
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <TouchableOpacity
              transparent
              onPress={() => this.props.navigation.navigate("HomeMenu")}
            >
              <Icon
                name="arrow-back"
                size={35}
                style={{ color: colors.black }}
              />
            </TouchableOpacity>
          </Left>
          <Body style={{ flex: 1, alignItems: "center" }}>
            <Title style={{ fontSize: 20, color: colors.black }}>
              List Client
            </Title>
          </Body>
          <Right style={{ flex: 1 }} />
        </Header>

        <TextInput
          style={styles.input}
          placeholder="Cari Client"
          onChangeText={text => this.componentDidMount(text)}
          value={this.state.searchWordClient}
        />

        <View style={{ marginTop: 5, flex: 1 }}>
          <FlatList
            data={this.state.listClient}
            renderItem={this._renderItem}
            numColumns={1}
            keyExtractor={(item, index) => index.toString()}
          />
          <Text>{this.state.text}</Text>
        </View>
      </Container>
    );
  }
}
