import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon
} from "native-base";
import colors from "../../../styles/colors";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/AllClient";
import Moment from "moment";

var that;
class ListItem extends React.PureComponent {
  navigateToScreen(route, date) {
    AsyncStorage.setItem("date", JSON.stringify(date)).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View style={[styles.container]}>
        <TouchableOpacity
          transparent
          onPress={() =>
            this.navigateToScreen("DetailHistory", this.props.data.day)
          }
        >
          <View
            style={{
              width: "100%",
              paddingTop: 5,
              paddingLeft: 10,
              paddingRight: 10,
              marginBottom: 3,
              borderRadius: 15
            }}
          >
            <Text
              style={{ fontSize: 20, color: colors.black, textAlign: "center" }}
            >
              <Icon
                type="MaterialIcons"
                name="date-range"
                style={{ color: "#212e59", fontSize: 20, marginTop: 5 }}
              />{" "}
              {"\t"}
              {Moment(this.props.data.day).format("DD MMMM YYYY")}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

export default class ListHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      active: "true",
      isLoading: false,
      search: "Search",
      listHistory: [],
      filter: "",
      idClient: ""
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    AsyncStorage.getItem("idClient")

      .then(idClient => {
        var url = GlobalConfig.SERVERHOST + "list-history-client/" + idClient;
        //alert(JSON.stringify(idClient));
        var formData = new FormData();
        formData.append();
        fetch(url, {
          headers: {
            "Content-Type": "multipart/form-data"
          },
          method: "POST",
          body: formData
        })
          .then(response => response.json())
          .then(responseJson => {
            //alert(JSON.stringify(responseJson));
            if (responseJson.status == 302) {
              this.setState({
                listHistory: responseJson.data,
                isLoading: false
              });
            }
          });
      })
      .catch(error => {
        console.log(error);
      });

    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {}
    );
  }

  _renderItem = ({ item }) => <ListItem data={item} />;

  render() {
    that = this;
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <TouchableOpacity
              transparent
              onPress={() => this.props.navigation.goBack()}
            >
              <Icon
                name="arrow-back"
                size={35}
                style={{ color: colors.black }}
              />
            </TouchableOpacity>
          </Left>
          <Body style={{ flex: 1, alignItems: "center" }}>
            <Title style={{ fontSize: 20, color: colors.black }}>History</Title>
          </Body>
          <Right style={{ flex: 1 }} />
        </Header>

        <View style={{ marginTop: 3, flex: 1, borderRadius: 10 }}>
          <FlatList
            data={this.state.listHistory}
            renderItem={this._renderItem}
            numColumns={1}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </Container>
    );
  }
}
