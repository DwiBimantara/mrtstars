import React, { Component } from "react";
import {
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  TouchableOpacity,
  Dimensions,
  ImageBackground,
  Platform,
  FlatList
} from "react-native";
import { Container, Header, Button, Card, Icon, Right } from "native-base";
import styles from "./styles/HomeMenu";
import colors from "../../styles/colors";
import CustomFooter from "../components/CustomFooter";
import SubClient from "../components/SubClient";
import GlobalConfig from "../components/GlobalConfig";

var that;
class ListClientt extends React.PureComponent {
  navigateToScreen(route, idListClient) {
    AsyncStorage.setItem("idListClient", JSON.stringify(idListClient)).then(
      () => {
        that.props.navigation.navigate(route);
      }
    );
  }

  render() {
    return (
      <View style={{ width: 300, paddingRight: 5 }}>
        <TouchableOpacity
          transparent
          onPress={() => this.navigateToScreen("DetailClient", this.props.data)}
        >
          <SubClient
            // imageUri={{ uri: GlobalConfig.IMAGEHOST + "Artikel/" + this.props.data.images }}
            nama={this.props.data.nama}
            pekerjaan={this.props.data.pekerjaan}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

export default class HomeMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      active: "true",
      isLoading: false,
      search: "Search",
      name: "",
      id: "",
      listClient: []
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    AsyncStorage.getItem("profil").then(dataUser => {
      this.setState({
        nama: JSON.parse(dataUser).nama,
        id: JSON.parse(dataUser).id
      });
      //alert(JSON.stringify(this.state.id))
      //this.loadData(JSON.parse(dataUser));
    });
    //.alert(JSON.stringify(this.state.id))
    //this.loadListClient()
    //alert(JSON.stringify(this.state.listClient))
  }

  loadListClient() {
    //alert(JSON.stringify(this.state.id))
    const url = GlobalConfig.SERVERHOST + "list-client/" + this.state.id;
    fetch(url, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      method: "GET"
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.code == 302) {
          this.setState({
            listClient: responseJson.data,
            isLoading: false
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  _renderClient = ({ item }) => <ListClientt data={item} />;

  navigateToScreen(route, idClient) {
    AsyncStorage.setItem("idClient", JSON.stringify(idClient)).then(() => {
      this.props.navigation.navigate(route);
    });
  }
  render() {
    that = this;
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Right style={{ flex: 2, marginLeft: 15 }}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("Account")}
            >
              <Image
                source={require("../../assets/images/Profil.png")}
                style={{ width: 30, height: 30, resizeMode: "contain" }}
              />
            </Button>
          </Right>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <ScrollView>
          <ImageBackground
            style={{
              alignSelf: "center",
              width: 270,
              height: 150
            }}
            source={require("../../assets/images/stars.png")}
          />
          <Card
            style={{
              marginLeft: 20,
              marginRight: 20,
              borderRadius: 20,
              marginTop: "20%",
              paddingBottom: 40
            }}
          >
            <View>
              <Text style={styles.signIn}>Halo, Agent {this.state.nama} !</Text>
            </View>
            <View
              style={{
                width: "100%",
                marginTop: 0,
                backgroundColor: colors.white
              }}
            >
              <View style={{ marginTop: 0 }}>
                <View style={{ flex: 1, flexDirection: "row", paddingTop: 20 }}>
                  <View
                    style={{
                      width: "100%",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <View
                      style={{
                        width: 80,
                        height: 80,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <TouchableOpacity
                        transparent
                        onPress={() =>
                          this.navigateToScreen("ListClient", this.state.id)
                        }
                      >
                        <Image
                          source={require("../../assets/images/group.png")}
                          style={{
                            width: 60,
                            height: 60,
                            resizeMode: "contain"
                          }}
                        />
                        <Text style={{ fontSize: 15, textAlign: "center" }}>
                          Client
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                  {/* <View style={{ width: '50%', justifyContent: "center", alignItems: "center" }}>
                      <View style={{ width: 80, height: 80, justifyContent: "center", alignItems: "center" }}>
                        <TouchableOpacity
                          transparent
                          onPress={() => this.navigateToScreen("", 2)}
                        >
                          <Image
                            source={require("../../assets/images/history.png")}
                            style={{ width: 40, height: 40, resizeMode: "contain" }}
                          />
                        </TouchableOpacity>
                      </View>
                      <Text style={{ fontSize: 10 }}>Account</Text>
                    </View> */}
                </View>
              </View>
            </View>
          </Card>

          {/* <View style={styles.title}>
            <View style={{width:'50%', paddingLeft:15}}>
              <Text style={styles.titleTextLeft}>Artikel</Text>
            </View>
            <View style={{width:'50%', paddingRight:15}}>
            </View>
          </View>

          <View style={{width:'100%', marginTop:0, paddingBottom:5, backgroundColor:colors.white, paddingLeft:15, paddingRight:15}}>
            <View style={{marginTop:0}}>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              <FlatList
                horizontal={true}
                data={this.state.listClient}
                renderItem={this._renderClient}
                keyExtractor={(item, index) => index.toString()}
              />
            </ScrollView>
            </View>
          </View> */}
        </ScrollView>
        <CustomFooter navigation={this.props.navigation} menu="Home" />
      </Container>
    );
  }
}
