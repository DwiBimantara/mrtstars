import React from 'react';
import {
    View,
    Text,
    Image,
    Platform,
    Animated,
    Easing,
    ScrollView
} from 'react-native';
import {
    Container,
    Header,
    Left,
    Right,
    Button,
    Body,
    Title,
    Icon,
} from 'native-base'

import styles from "./styles/SplashScreen";
import colors from "../../styles/colors";
import RoundedButton from "../components/RoundedButton";

class SplashScreen extends React.Component {
    constructor(props) {
        super(props);
        this.animatedImage = new Animated.Value(0.2);
        this.animatedText = new Animated.Value(0);
    }

    static navigationOptions = {
        header: null
    };

    performTimeConsumingTask = async () => {
        return new Promise((resolve) =>
            setTimeout(
                () => { resolve('result') },
                //2000
                500
            )
        )
    }

    async componentDidMount() {
        Animated.spring(this.animatedImage, {
            toValue: 1,
            friction: 4,
            delay: 200,
            duration: 8000,
            useNativeDriver: true,
        }).start();

        Animated.timing(this.animatedText, {
            toValue: 1,
            delay: 200,
            duration: 800,
            useNativeDriver: true,
        }).start();

        const data = await this.performTimeConsumingTask();

        if (data !== null) {
            this.props.navigation.navigate('SplashScreen');
        }
    }

    onLoginPress = () => {
        this.props.navigation.navigate("Login");
    }

    render() {
        const imageStyle = {
            transform: [{ scale: this.animatedImage }]
        };

        const buttonStyle = {
            transform: [{ scale: this.animatedText }]
        };

        return (
            <Container style={styles.wrapper}>
                <ScrollView>
                    <View style={styles.iconLogo}>
                        <Animated.View style={[styles.ring, imageStyle]}>
                            <Animated.Image
                                source={require('../../assets/images/stars.png')}
                                style={[
                                    {
                                        resizeMode: "contain",
                                        width: 250,
                                        height: 180
                                    }
                                ]}
                            />
                        </Animated.View>
                    </View>
                    <Animated.View style={[styles.buttonLogin, buttonStyle]}>
                        <RoundedButton
                            text="LOGIN"
                            textColor={colors.black}
                            background={colors.grayprimary}
                            handleOnPress={this.onLoginPress}
                            style={styles.loginButton}
                        />
                        <RoundedButton
                            text="LOGIN"
                            textColor={colors.white}
                            background={colors.white}                          
                            style={styles.loginButton}
                        />
                        
                    </Animated.View>
                </ScrollView>
            </Container>
        );
    }
}


export default SplashScreen;