import { StyleSheet } from "react-native";
import iPhoneSize from "../../../helpers/utils";
import colors from "../../../styles/colors";
import { Row } from "native-base";

let labelTextSize = 12;
let weatherTextSize = 18;
let weatherTextSmSize = 14;
let headingTextSize = 28;
if (iPhoneSize() === "small") {
  labelTextSize = 10;
  headingTextSize = 24;
  weatherTextSize = 16;
  weatherTextSmSize = 12;
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    display: "flex",
    backgroundColor:colors.gray
  },
  header: {
    backgroundColor: colors.white
  },
  btnOrder: {
    paddingTop:10,
    paddingBottom:10,
    borderRadius:30,
    width:'100%',
    marginRight:10,
    backgroundColor:colors.secondary,
    alignItems: 'center',
    justifyContent: 'center'
  },
  textOrder: {
    fontSize:18,
    fontWeight:'bold',
    color:colors.white
  },
  tabfooter: {
    backgroundColor: colors.white
  },
  placeholder: {
    backgroundColor: "#eee",
    width: 80,
    height: 80,
    borderWidth:1,
    borderColor:colors.black,
    borderStyle:'dashed',
    borderRadius:5,
  },
  previewImage: {
    width: "100%",
    height: "100%",
    borderRadius:5,
  },


});

export default styles;