import { StyleSheet } from "react-native";
import iPhoneSize from "../../../helpers/utils";
import colors from "../../../styles/colors";
import { Row } from "native-base";

let labelTextSize = 12;
let weatherTextSize = 18;
let weatherTextSmSize = 14;
let headingTextSize = 28;
if (iPhoneSize() === "small") {
  labelTextSize = 10;
  headingTextSize = 24;
  weatherTextSize = 16;
  weatherTextSmSize = 12;
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    display: "flex",
    backgroundColor: colors.gray
  },
  header: {
    backgroundColor: colors.white
  },
  homeWrapper: {
    flex: 1,
    display: "flex"
  },
  searchHeader: {
    width: "100%",
    height: 35,
    borderRadius: 5,
    marginLeft: 10,
    backgroundColor: colors.gray
  },
  iconHeader: {
    color: colors.black,
    fontSize: 20
  },
  kategori: {
    marginTop: 3,
    backgroundColor: colors.white
  },
  itemKategori: {
    flexDirection: "row",
    marginTop: 0,
    marginBottom: 5,
    justifyContent: "center"
  },
  populer: {
    marginTop: 0,
    backgroundColor: colors.white,
    paddingBottom: 5
  },
  itemPopuler: {
    flexDirection: "row",
    marginTop: 5,
    justifyContent: "center"
  },
  flashSale: {
    marginTop: 0,
    backgroundColor: colors.white
  },
  itemFlashSale: {
    flexDirection: "row",
    marginTop: 0,
    justifyContent: "center"
  },
  produk: {
    marginTop: 0,
    backgroundColor: colors.gray
  },
  itemProduk: {
    flexDirection: "row",
    marginTop: 0,
    justifyContent: "center"
  },
  title: {
    flex: 1,
    flexDirection: "row",
    marginTop: 3,
    backgroundColor: colors.white,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 10,
    paddingRight: 10
  },
  title2: {
    flex: 1,
    flexDirection: "row",
    marginTop: 0,
    backgroundColor: colors.white,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 10,
    paddingRight: 10
  },
  titleTextLeft: {
    fontSize: 12,
    fontWeight: "bold",
    color: colors.secondary
  },
  titleTextRight: {
    fontSize: 12,
    textAlign: "right",
    color: colors.secondary
  },
  titleInput: {
    fontSize: 10
  },
  placeholder: {
    backgroundColor: "#eee",
    width: 80,
    height: 80,
    borderWidth: 1,
    borderColor: colors.black,
    borderStyle: "dashed",
    borderRadius: 5
  },
  previewImage: {
    width: "100%",
    height: "100%",
    borderRadius: 5
  },
  btnQTYLeft: {
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    width: 30,
    height: 30,
    backgroundColor: colors.secondary,
    marginTop: 10
  },
  btnQTYRight: {
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    width: 30,
    height: 30,
    backgroundColor: colors.secondary,
    marginTop: 10
  },
  facebookButtonIconQTY: {
    color: colors.white,
    fontSize: 20,
    paddingTop: 5,
    width: 30,
    height: 30
  },
  container: {
    flex: 1,
    display: "flex",
    width: "100%",
    height: 40,
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 5,
    paddingBottom: 5,
    backgroundColor: colors.white,
    borderRadius: 10
  },

  weatherContent: {
    flexDirection: "row",
    // marginBottom: 15,
    borderBottomColor: colors.lightBlack,
    borderBottomWidth: 1,
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20
  },
  newsContent: {
    flexDirection: "row"
  },
  weatherText: {
    fontFamily: "Montserrat-Medium",
    fontSize: weatherTextSize,
    color: colors.lightBlack,
    fontWeight: "300",
    marginBottom: 5
  },
  newsKatText: {
    fontFamily: "Montserrat-Medium",
    fontSize: 10,
    marginTop: 5,
    color: colors.lightBlack,
    fontWeight: "300"
  },
  newsTitleText: {
    // fontFamily: "Montserrat-Regular",
    fontSize: 24,
    marginTop: 5,
    color: colors.black,
    fontWeight: "bold"
  },
  newsTextDetail: {
    fontFamily: "Montserrat-Medium",
    fontSize: 14,
    marginTop: 5,
    color: colors.black,
    fontWeight: "300"
  },
  newsTanggalDetail: {
    fontFamily: "Montserrat-Medium",
    fontSize: 12,
    marginTop: 5,
    color: colors.lightBlack,
    fontWeight: "300"
  },
  newsKatDetail: {
    fontFamily: "Montserrat-Medium",
    fontSize: 12,
    marginTop: 5,
    color: colors.green0,
    fontWeight: "300"
  },
  newsKatContent: {
    borderTopColor: colors.lightBlack,
    borderTopWidth: 1,
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 5
  },
  weatherTextSm: {
    fontFamily: "Montserrat-Light",
    fontSize: weatherTextSmSize,
    color: colors.lightBlack,
    fontWeight: "300",
    marginBottom: 10
  },
  newsText: {
    fontFamily: "Montserrat-Regular",
    fontSize: weatherTextSmSize,
    color: colors.lightBlack,
    fontWeight: "300",
    marginBottom: 5,
    marginLeft: 20
  },
  news: {
    height: 160,
    marginTop: 20
  }
});

export default styles;
