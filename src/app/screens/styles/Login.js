import { StyleSheet } from "react-native";
import colors from "../../../styles/colors";
import { Row } from "native-base";

let labelTextSize = 12;
let weatherTextSize = 18;
let weatherTextSmSize = 14;
let headingTextSize = 28;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    display: "flex"
  },
  homeWrapper: {
    flex: 1,
    display: "flex"
  },
  logo: {
    width: 60,
    height: 70,
    marginTop: 10,
    marginBottom: 40
  },
  scrollView: {
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 20,
    flex: 1
  },
  scrollViewWrapper: {
    marginTop: 30,
    flex: 1,
    padding: 0,
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0
  },
  titleLogin: {
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    backgroundColor: colors.primary,
    height: 60
  },
  signIn: {
    flex: 1,
    fontFamily: "Montserrat-Medium",
    fontSize: 20,
    color: colors.black,
    fontWeight: "300",
    marginTop: 10,
    textAlign: "center"
  },
  weatherText: {
    fontFamily: "Montserrat-Medium",
    fontSize: labelTextSize,
    color: colors.lightBlack,
    fontWeight: "600",
    marginBottom: 5
  },
  weatherTextLink: {
    fontFamily: "Montserrat-Medium",
    fontSize: labelTextSize,
    color: colors.secondary,
    fontWeight: "bold",
    textAlign: "center"
  },
  footerLogo: {
    marginLeft: 10,
    width: 30,
    height: 30,
    backgroundColor: colors.secondary,
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center"
  },
  placeholder: {
    backgroundColor: "#eee",
    width: 100,
    height: 100,
    borderRadius: 100
  },
  previewImage: {
    width: "100%",
    height: "100%",
    borderRadius: 100
  }
});

export default styles;
