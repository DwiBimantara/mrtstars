import { StyleSheet } from "react-native";
import colors from "../../../styles/colors";

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    display: "flex",
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.white
  },
  iconLogo: {
    flex:1,
    display: "flex",
    alignItems: 'center',
    justifyContent: 'center',
    marginTop:'40%'
  },
  logo:{
    width:120,
    height:100
  },
  buttonLogin:{
    marginTop:"30%",
    marginLeft:20,
    marginRight:20
  }

});

export default styles;
